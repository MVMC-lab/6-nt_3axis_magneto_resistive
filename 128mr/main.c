//GET SOUND DATA 5K
#include <avr\io.h>
#include <avr\interrupt.h>
#include <stdio.h>
#include <util\delay.h>
#include <string.h>

#include "HMI/ASA_lib_HMI.h"
//HMI

#define HMI_TYPE_I8   0
#define HMI_TYPE_I16  1
#define HMI_TYPE_I32  2
#define HMI_TYPE_I64  3
#define HMI_TYPE_UI8  4
#define HMI_TYPE_UI16 5
#define HMI_TYPE_UI32 6
#define HMI_TYPE_UI64 7
#define HMI_TYPE_F32  8
#define HMI_TYPE_F64  9


//UART DEF
#define HEADER 0xAA
#define UDR	    UDR1
#define UCSRA   UCSR1A
#define UCSRB   UCSR1B
#define UCSRC   UCSR1C
#define UBRRH   UBRR1H
#define UBRRL   UBRR1L
#define USART_RX_vect USART1_RX_vect
#define USART_DDR   DDRD
#define USART_PORT  PORTD
#define USART_RX    PD2
#define USART_TX    PD3

//UART FUN
void trm(char *Data_p){
    char data = *((char*)Data_p);
    while (!( UCSR1A & (1<<UDRE1)));
    UDR1 = data;

}
char rec(char *Data_p,int timeout){
    uint32_t timeoutcount=0;
    uint32_t countpersec=11000;//1100=1ms
    uint32_t timeoutcountmax=timeout*countpersec;
    //
    while ( !(UCSR1A & (1<<RXC1)) ){
        timeoutcount++;
        if(timeoutcount>=timeoutcountmax){
            *((char*)Data_p) = 0;
            break;
        }
    }
    if(timeoutcount>=timeoutcountmax){
        return 2;
    }
    *(Data_p) = UDR1;
    return 0;

}
char M128_UARTM_trm(char Mode, char UartID, char RegAdd, char Bytes, void *Data_p)
{
    if(Mode<0 || Mode>14)       return 1;
    if(UartID<0)			    return 2;
    //if(RAdd<0)			    return 3;
    if(RegAdd<0 || RegAdd>0x80)	return 3;
    if(Bytes<0)			        return 4;
    char trans_temp=0;
    char reciv_temp=0;
    uint8_t check,checksum = 0;
    int i=0;
    switch(Mode){
        case 0:{
            /*
            DDRF  |= 0X10;								//RS-485 use ASA 20PIN.DIO0
            RS485_WRITE;								//DIO0  1:write  0:read
            check = M128_UART_put(3, 1, &Header);					//send Header
            if(check) return 5;
            check = M128_UART_put(3, 1, &UID);						//send UID
            if(check) return 5;
            trans_temp = (RAdd | 0x80);
            check = M128_UART_put(3, 1, &trans_temp);				//send RAdd
            if(check) return 5;
            checksum = UID + trans_temp;
            for(int i=0;i<Bytes;i++)
            {
                check = M128_UART_put(3, 1, (char*)Data_p+i);		//send Data
                if(check) return 5;
                checksum += *((char*)Data_p+i);
            }
            check = M128_UART_put(3, 1, &checksum);
            if(check) return 5;
            RS485_READ;							//DIO0  1:write  0:read
            check = M128_UART_get(3, 1, &get_back_data);		//receive Data
            if(check) return 5;
            break;
            */
            //Mode 0
            //header UartID RADD DATA REC(header)
            //header S=Header  ChkS=0
            checksum=0;
            trans_temp=HEADER;
            trm(&trans_temp);
            //printf("header\n" );
            //uid S=UID, ChkS=ChkS+S
            trans_temp=UartID;
            trm(&trans_temp);
            checksum+=trans_temp;
            //printf("UID\n" );

            //radd S=R+Add,ChkS=ChkS+S
            trans_temp=RegAdd;
            trm(&trans_temp);
            checksum+=trans_temp;
            //printf("RADD\n" );
            //data  FOR i=0:Byte-1
            //S=*Data_p+i
            //ChkS=ChkS+S
            //ENDFOR
            for( i = 0 ; i < Bytes ; ++i )
            {
                trans_temp=*((uint8_t *)Data_p+i);
                trm(&trans_temp);
                checksum+=trans_temp;
            }
            //chks S=ChkS,
            trans_temp=checksum;
            trm(&trans_temp);
            //polling HEADER
            //printf("wait\n" );
            check=rec(&reciv_temp,1);
            if(check) {return 5;}//Return(TOut)
            if(reciv_temp!=HEADER) {return 6;}
            //printf("%d;;;;%d\n",check,reciv_temp );
            return 0;//Return(OK)
        }//case 0


    }//switch(Mode)
}
char M128_UARTM_rec(char Mode,char UartID, char RegAdd, char Bytes, void *Data_p)
{
    if(Mode<0 || Mode>14)    return 1;
    if(UartID<0)			    return 2;
    //if(RAdd<0)			return 3;
    if(RegAdd<0 || RegAdd>0x80)	return 3;
    if(Bytes<0)			    return 4;
    char trans_temp=0;
    char reciv_temp=0;
    uint8_t check,checksum = 0;
    uint8_t ChkSR=0;
    uint8_t uarts_data_temp[16]={0};
    char i=0;
    switch(Mode){
        case 0:{
            /*
            DDRF  |= 0X10;								//RS-485 use ASA 20PIN.DIO0
            RS485_WRITE;									//DIO0  1:write  0:read
            check = M128_UART_put(3, 1, &Header);					//send Header
            if(check) return check;
            check = M128_UART_put(3, 1, &UID);						//send UID
            if(check) return check;
            trans_temp = (RAdd & 0x7F);
            check = M128_UART_put(3, 1, &trans_temp);				//send RAdd
            if(check) return check;
            checksum = UID + trans_temp;
            check = M128_UART_put(3, 1, &checksum);					//send checksum
            if(check) return check;
            RS485_READ;
            check = M128_UART_get(3, 1, &Header);					//receive header
            if(check) return check;
            for(int i=0;i<Bytes;i++)
            {
                check = M128_UART_get(3, 1, (char*)Data_p+i);		//receive Data
                if(check) return check;
                checksum += *((char*)Data_p+i);
            }
            check = M128_UART_get(3, 1, &Header);					//receive checksum
            if(check) return check;

            break;
            */
            //Mode 0
            //header UartID RADD (header)(DATA) REC(header) REC(OK)
            //header S=Header  ChkS=0
            checksum=0;
            trans_temp=HEADER;
            trm(&trans_temp);
            //uid S=UID, ChkS=ChkS+S
            trans_temp=UartID;
            trm(&trans_temp);
            checksum+=trans_temp;
            //radd  S=R+Add,ChkS=ChkS+S
            RegAdd=RegAdd|0x80;
            trans_temp=RegAdd;
            trm(&trans_temp);
            checksum+=trans_temp;
            //checksum  S=ChkS, i=0
            trans_temp=checksum;
            trm(&trans_temp);

            //polling HEADER Polling
            //For Header=G
            check=rec(&reciv_temp,1);
            if(check) {return 5;}
            if(reciv_temp!=HEADER) {return 6;}
            //ChkS=0
            checksum=0;
            //polling data
            // For i=0: Bytes-1
            // Polling for
            // TMP(i)=G
            // ChkS=ChkS+G
            // ENDFOR
            for( i = 0 ; i < Bytes ; ++i )
            {
                check=rec(&uarts_data_temp[i],1);
                if(check) {return 5;}
                checksum+=uarts_data_temp[i];
            }
            //Polling For=G
            // ChkSR=G
            check=rec(&reciv_temp,1);
            ChkSR=reciv_temp;
            if(check) {return 5;}
            if(ChkSR!=checksum) {return 6;}//ChkSR ?=ChkS
            //chks is good
            // For i=0: Bytes
            // *Data_p+i=Tmp(i)
            // ENDFOR
            // Return(OK)
            for( i = 0 ; i < Bytes ; ++i )
            {
                *((uint8_t *)Data_p+i)=uarts_data_temp[i];
            }
            //all ok
            return 0;
        }//case0
    }//switch(mode)
}
void M128_UART_set(void){
    UBRRH = 0;
    UBRRL = 17;
    UCSRB = 0b00011000;
    UCSRC = 0b00000110;
    USART_DDR |= (1<<USART_TX);
    USART_DDR &=~(1<<USART_RX);
    DDRB=(7<<5);
    PORTB=(1<<5);
}
//PRINTF
#define DEFAULTUARTBAUD 38400
int stdio_putchar(char c, FILE *stream);
int stdio_getchar(FILE *stream);
static FILE STDIO_BUFFER = FDEV_SETUP_STREAM(stdio_putchar, stdio_getchar, _FDEV_SETUP_RW);
int stdio_putchar(char c, FILE *stream){
    if (c == '\n')
        stdio_putchar('\r',stream);

    while((UCSR0A&(1<<UDRE0))==0);

    UDR0 = c;

    return 0;
}
int stdio_getchar(FILE *stream){
	int UDR_Buff;

    while((UCSR0A&(1<<RXC0))==0);

	UDR_Buff = UDR0;

	stdio_putchar(UDR_Buff,stream);

	return UDR_Buff;
}
char ASA_M128_set(void){
	unsigned int baud;

	baud = F_CPU/16/DEFAULTUARTBAUD-1;
	UBRR0H = (unsigned char)(baud>>8);
	UBRR0L = (unsigned char)baud;

	UCSR0B |= (1<<RXEN0) | (1<<TXEN0);
	UCSR0C |= (3<<UCSZ00);

	DDRB |= (0x07 << PB5);

	stdout = &STDIO_BUFFER;
	stdin = &STDIO_BUFFER;

	return 0;
}

uint16_t mrdata[10];
void getmadata(void){
    char regadd,err;
    uint16_t tempdata;
    for(int i=1;i<=9;i++){//1~3
        regadd=i;
        err=1;
        while(err==1){
            _delay_ms(10);
            err=M128_UARTM_rec(0,0,regadd,2,&tempdata);
        }
        if(tempdata<=10){err=1;}
        mrdata[i]=tempdata;
        tempdata=0;
    }
}

int main(void){
    ASA_M128_set();
    char s[20];
    M128_UART_set();
    sei();
    getmadata();
    printf("mrdata get\n");


    while(1){
            _delay_ms(100);getmadata();
            printf("send mr data\n");
            scanf("%s");//fprintf(port, 'Hello');
            for(int i=4;i<=9;i++){
                M128_HMI_put(2 , HMI_TYPE_UI16 , mrdata[i] );
            }


    }
}
