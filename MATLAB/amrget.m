
% clear all
clc; clear;
vgain=0.0038   %mv
tgain=0.2375*8 %nt
max=10000
% open Serial Port
port_num = 4
port = REMO_open(port_num)

% M128 send string printf("mrdata get\n");
s1 = fscanf(port,'%s')

for i=1:max
    % start get mrdata
    % M128 send string printf("send mr data\n");
    s2 = fscanf(port,'%s');

    % send string
    fprintf(port, 'Hello');
    res = fscanf(port,'%s');

    % get matrix data
    type =5; bytes = 2; % uint16=2
    x(1,i)  = REMO_get(port, type, bytes);
    x(2,i)  = REMO_get(port, type, bytes);
    y(1,i)  = REMO_get(port, type, bytes);
    y(2,i)  = REMO_get(port, type, bytes);
    z(1,i)  = REMO_get(port, type, bytes);
    z(2,i)  = REMO_get(port, type, bytes);
    x(3,i)=-x(1,i)+x(2,i)
    y(3,i)=y(1,i)-y(2,i)
    z(3,i)=z(1,i)-z(2,i)
    
    figure(1);
    subplot(2,2,1) 
    xlabel('X軸磁場強度') 
    ylabel('Y軸磁場強度') 
    zlabel('Z軸磁場強度') 
    quiver3(0,x(3,i),0, y(3,i),0, z(3,i))
    axis([-20000 20000 -20000 20000 -20000 20000])
    grid on
    subplot(2,2,2)
    xlabel('X軸磁場強度') 
    ylabel('Y軸磁場強度') 
    quiver(x(3,i), y(3,i))
    axis([-20000 20000 -20000 20000])
    grid on
    subplot(2,2,3)
    xlabel('X軸磁場強度') 
    ylabel('Z軸磁場強度') 
    quiver(x(3,i), z(3,i))
    axis([-20000 20000 -20000 20000])
    grid on
    subplot(2,2,4)
    xlabel('Y軸磁場強度') 
    ylabel('Z軸磁場強度') 
    quiver(y(3,i), z(3,i))
    axis([-20000 20000 -20000 20000])
    grid on
    
end
REMO_close(port);


figure(2);
plot(x(3,1:max)*tgain,x(1,1:max)*vgain,'x',x(3,1:max)*tgain,x(2,1:max)*vgain,'o');
axis([-50000 50000 0 125])
xlabel('X軸磁場強度(nT)') 
ylabel('MR IC輸出電壓(mv)')
grid on
figure(3);
plot(y(3,1:max)*tgain,y(1,1:max)*vgain,'x',y(3,1:max)*tgain,y(2,1:max)*vgain,'o');
axis([-50000 50000 0 125])
xlabel('Y軸磁場強度(nT)') 
ylabel('MR IC輸出電壓(mv)')
grid on
figure(4);
plot(z(3,1:max)*tgain,z(1,1:max)*vgain,'x',z(3,1:max)*tgain,z(2,1:max)*vgain,'o');
axis([-50000 50000 0 125])
xlabel('Z軸磁場強度') 
ylabel('MR IC輸出電壓')
grid on

