//per 100ms get mrdata
//get mrdata spend 0.5ms
//uart spend 2ms

#include <avr\io.h>
#include <avr\interrupt.h>
#include <stdio.h>
#include <util\delay.h>
//REG
typedef struct {
    void*   ptr;
    uint8_t bytes;
} register_ptr_str;
volatile register_ptr_str reg[10];

void regset(void){

    reg[0].ptr=&getdataflag;
    reg[0].bytes=1;
    reg[1].ptr=&mrdata[0];//X mr data
    reg[1].bytes=2;
    reg[2].ptr=&mrdata[1];//Y mr data
    reg[2].bytes=2;
    reg[3].ptr=&mrdata[2];//Z mr data
    reg[3].bytes=2;

    reg[4].ptr=&mrdatatemp[0];//XSET
    reg[4].bytes=2;
    reg[5].ptr=&mrdatatemp[3];//XRSET
    reg[5].bytes=2;
    reg[6].ptr=&mrdatatemp[1];//YSET
    reg[6].bytes=2;
    reg[7].ptr=&mrdatatemp[4];//YRSET
    reg[7].bytes=2;
    reg[8].ptr=&mrdatatemp[2];//ZSET
    reg[8].bytes=2;
    reg[9].ptr=&mrdatatemp[5];//ZRSET
    reg[9].bytes=2;
}
