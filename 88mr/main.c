//per 100ms get mrdata
//get mrdata spend 0.5ms+1ms(wait)
//uart spend 2ms

#include <avr\io.h>
#include <avr\interrupt.h>
#include <stdio.h>
#include <util\delay.h>
//REG
typedef struct {
    void*   ptr;
    uint8_t bytes;
} register_ptr_str;
volatile register_ptr_str reg[10];



//UART
#define HEADER 0xAA

#define Uart_rx_state_HEADER 0
#define Uart_rx_state_UID    1
#define Uart_rx_state_ADDR   2
#define Uart_rx_state_BYTES  3
#define Uart_rx_state_DATA   4
#define Uart_rx_state_CHKSUM 5

#define Uart_tx_state_HEADER 0
#define Uart_tx_state_DATA    1
#define Uart_tx_state_CHKS   2

#define Maxtemp 16

#define Maxtimeout_count 20000//10ms
#define P_UID 0

typedef struct Uarts_Str{
   volatile uint8_t uarts_rx_state;
   volatile uint8_t uarts_tx_state;
   volatile uint8_t uarts_chks;
   volatile uint8_t uarts_UID;
   volatile uint8_t uarts_addr;
   volatile uint8_t uarts_WR;
   volatile uint8_t uarts_byte_count;
   volatile uint8_t uarts_totalbytes;
   volatile uint8_t uarts_masterchk;
   volatile uint32_t timeout_count;
   volatile uint8_t uarts_data_temp[Maxtemp];
} uarts_str_;
volatile uarts_str_ uarts_str;

//MR
//this for PC0:2
//PC0 74139 A
//PC1 74139 B
//0=Y0 1=Y1 2=Y2
//PC2 set/rset
#define set 3
#define setx 0
#define sety 1
#define setz 2
#define rset 7
#define rsetx 4
#define rsety 5
#define rsetz 6
volatile char getdataflag=0;
volatile int uart_get_data=0;
volatile unsigned int mrdatatemp[7]={0};
volatile unsigned int mrdata[10]={0};

void regset(void){

    reg[0].ptr=&getdataflag;
    reg[0].bytes=1;
    reg[1].ptr=&mrdata[0];//X mr data
    reg[1].bytes=2;
    reg[2].ptr=&mrdata[1];//Y mr data
    reg[2].bytes=2;
    reg[3].ptr=&mrdata[2];//Z mr data
    reg[3].bytes=2;

    reg[4].ptr=&mrdatatemp[0];//XSET
    reg[4].bytes=2;
    reg[5].ptr=&mrdatatemp[3];//XRSET
    reg[5].bytes=2;
    reg[6].ptr=&mrdatatemp[1];//YSET
    reg[6].bytes=2;
    reg[7].ptr=&mrdatatemp[4];//YRSET
    reg[7].bytes=2;
    reg[8].ptr=&mrdatatemp[2];//ZSET
    reg[8].bytes=2;
    reg[9].ptr=&mrdatatemp[5];//ZRSET
    reg[9].bytes=2;
}

int main(){
    regset();
	cli();
    DDRD=2;
    PORTD=2;
    _delay_ms(1);
    PORTD=0;

	//DDRD=3;
	DDRC=(1<<0)|(1<<1)|(1<<2);//set rest & cs of ADC
	PORTC=rset;
	spi_init();
    USART_set();
    timer_init();
	sei();

    getdata();
	while(1){
        UARTS_timeout();
		if(getdataflag==1)
		{
			//PORTD=2;
			getdata();
			getdataflag=0;
			//PORTD=0;
		}
	}

}

//UART
void USART_set(void){
    // 12bits with U2X = 0 UBRR = 8000000/16/(BAUD = 38400)-1 = 12
    DDRD  &=~(1<<PD0);//PD0(RX) as input
    DDRD  |= (1<<PD1);//PD0(TX) as output
    // UBRR = [8000000/16/(BAUD = 38400)]-1 = 12
    UBRR0H = 0;
    UBRR0L = 12;
    //RX TX interrupt open RX TX enable
    UCSR0B = (1<<RXCIE0)|(1<<TXCIE0)|(1<<RXEN0)|(1<<TXEN0);
    //Character Size=8bit
    UCSR0C = (3<<UCSZ00);
}
void UARTS_rx(void){
    //PORTB=1<<4;
    uint8_t uarts_datain;
    uarts_datain=UDR0;
// if(uarts_str.uarts_rx_state==Uart_rx_state_HEADER){PORTB=1<<4;}
// else{PORTB=0;}
 PORTB=1<<4;
    switch (uarts_str.uarts_rx_state) {
        case Uart_rx_state_HEADER:{
            //TOCount=TOut;(逾時計數器恢復)
            uarts_str.timeout_count =Maxtimeout_count;

            //讀取UART收值與正確領頭碼比判斷是否切換狀態(參考編號1)
            if(uarts_datain == HEADER) {//PORTB=0;
                uarts_str.uarts_rx_state = Uart_rx_state_UID;
                uarts_str.uarts_chks = 0;
                PORTB=0;

            }
            // else return HEADER_NOT_MATCH;
            break;
            //PORTB=0;
        }
        case Uart_rx_state_UID:{//PORTB=0;
            //TOCount=TOut;(逾時計數器恢復)
            uarts_str.timeout_count =Maxtimeout_count;
            //    • 讀取UART收值，做為新UID
            uarts_str.uarts_UID = uarts_datain;
            //    • 依據UID與本裝置是否吻合判斷是否切換狀態(參考編號2)
            if(uarts_str.uarts_UID==P_UID){
                uarts_str.uarts_rx_state = Uart_rx_state_ADDR;
                //uarts_str.uarts_chks += uarts_datain;
            }
            //      前狀態結束動作及後狀態初始動作
            //    • ChkSum=UID
            if(uarts_str.uarts_rx_state==Uart_rx_state_ADDR){
                uarts_str.uarts_chks=uarts_str.uarts_UID;
            }
            //UDR=uarts_datain;
            break;
        }
        case Uart_rx_state_ADDR:{//PORTB=1<<4;
            //TOCount=TOut;(逾時計數器恢復)
            uarts_str.timeout_count =Maxtimeout_count;
            //    • 讀取UART收值，分解取bit7為RW，bit6:0為暫存器編號。
            uarts_str.uarts_addr = uarts_datain & 0x7F;
            uarts_str.uarts_WR = ((uarts_datain & 0x80)>>7);
            //    • ChkSum=ChkSum+收值。
            uarts_str.uarts_chks += uarts_datain;
            //    • 檢查RW值決定切換狀態。(參考編號3 或4)
            if((uarts_str.uarts_WR)==0){//PORTB=0;//uarts_str.uarts_WR=write
                uarts_str.uarts_rx_state = Uart_rx_state_DATA;
                //      前狀態結束動作及後狀態初始動作
                //    • ByteCount =0
                uarts_str.uarts_byte_count=0;
                // UDR=0x33;
            }
            else if((uarts_str.uarts_WR)==1){//PORTB=1<<4;//uarts_str.uarts_WR=read
                uarts_str.uarts_rx_state = Uart_rx_state_CHKSUM;
                // UDR=0x66;
            }
            //UDR=uarts_str.uarts_WR;
            break;
        }
        case Uart_rx_state_DATA:{//PORTB=1<<4;

            //TOCount=TOut;(逾時計數器恢復)
            uarts_str.timeout_count =Maxtimeout_count;
            //    • 讀取UART收值，轉存入BUFF(ByteCount  )中。
            uarts_str.uarts_data_temp[uarts_str.uarts_byte_count]= uarts_datain;
            //    • ChkSum=ChkSum+收值。

            uarts_str.uarts_chks += uarts_datain;
            //    • ByteCount =ByteCount +1
            uarts_str.uarts_byte_count ++;

            //    • TotalBytes= RemoRW_reg表第暫存器編號個暫存器Byte數
            uarts_str.uarts_totalbytes=reg[uarts_str.uarts_addr].bytes;
            //UDR=uarts_str.uarts_totalbytes;
            //    • If(ByteCount  ==TotalBytes) ByteCount =0
            if(uarts_str.uarts_byte_count == uarts_str.uarts_totalbytes){
                // UDR=0x87;
                uarts_str.uarts_byte_count = 0;
            }
            else{
                //UDR=0x22;
            }
            //    • 檢查ByteCount 值決定切換狀態。 (參考編號5)
            if(uarts_str.uarts_byte_count == 0){
                uarts_str.uarts_rx_state = Uart_rx_state_CHKSUM;
            }

            break;
        }
        case Uart_rx_state_CHKSUM:{//PORTB=1<<4;
            //TOCount=TOut;(逾時計數器恢復)
            uarts_str.timeout_count =Maxtimeout_count;
            //    • 讀取UART收值，轉存入主端ChkSum 中。
            uarts_str.uarts_masterchk=uarts_datain;
            //比較主端ChkSum與ChkSum決定切換路徑。 (參考編號6，編號8)
            if(uarts_str.uarts_masterchk == uarts_str.uarts_chks){
                uarts_str.uarts_rx_state = Uart_rx_state_HEADER;
                {//前狀態結束動作及後狀態初始動作
                    if(uarts_str.uarts_WR==0){//    • IF(RW變數==寫入)
                        for(uarts_str.uarts_byte_count=0;uarts_str.uarts_byte_count<uarts_str.uarts_totalbytes;uarts_str.uarts_byte_count++){//        ◦ For(ByteCount=0到 TotalBytes )
                            //      讀取BUFF(ByteCount)轉存入(RemoRW_reg表第暫存器編號個暫存器住址 +ByteCount )位置。
                            *((uint8_t *) (reg[uarts_str.uarts_addr].ptr + uarts_str.uarts_byte_count )) = uarts_str.uarts_data_temp[uarts_str.uarts_byte_count];
                            UARTS_tx();
                        }
                    }
                    else if(uarts_str.uarts_WR==1){//    • IF(RW變數==讀取)
                        UARTS_tx();
                    }
                }//前狀態結束動作及後狀態初始動作
            }//if(uarts_str.uarts_masterchk == uarts_str.uarts_chks)
            else if(uarts_str.uarts_masterchk != uarts_str.uarts_chks){
                uarts_str.uarts_rx_state = Uart_rx_state_HEADER;
                //前狀態結束動作及後狀態初始動作
                //(清除 BUFF)
                uarts_str.uarts_totalbytes=0;
            }
            //else if(uarts_str.uarts_masterchk != uarts_str.uarts_chks){
            break;
        }
        default:{
            break;
        }
    }//switch


}
void UARTS_tx(void){
    uint8_t uarts_dataout;
    switch (uarts_str.uarts_tx_state) {
            case Uart_tx_state_HEADER:{//header
            uarts_dataout=HEADER;
            UDR0=uarts_dataout;
            if(uarts_str.uarts_WR==1){//read
                uarts_str.uarts_tx_state=Uart_tx_state_DATA;
                //前狀態結束動作及後狀態初始動作
                uarts_str.uarts_byte_count=0;
                uarts_str.uarts_chks=0;
            }
            else{
                uarts_str.uarts_tx_state=3;
            }
            break;
        }
        case Uart_tx_state_DATA:{//data
            //    • 讀取(RemoRW_reg表第暫存器編號個暫存器住址 + ByteCount )位置值取得變數Data
             uarts_dataout= *((uint8_t *) (reg[uarts_str.uarts_addr].ptr + uarts_str.uarts_byte_count ));
            //    • Data轉存UART硬體暫存器送出。
            UDR0=uarts_dataout;
            //    • CheckSum =CheckSum +Data
            uarts_str.uarts_chks+=uarts_dataout;
            //    • ByteCount =ByteCount +1
            uarts_str.uarts_byte_count++;
            //    • 檢查 ByteCount 是否RemoRW_reg表第暫存器編號個暫存器Byte數 相等，決定切換狀態。 (參考編號2)
            if(uarts_str.uarts_byte_count>=reg[uarts_str.uarts_addr].bytes){
                uarts_str.uarts_tx_state=Uart_tx_state_CHKS;
            }
            break;
        }
        case Uart_tx_state_CHKS:{//chksum
            uarts_dataout=uarts_str.uarts_chks;
            UDR0=uarts_dataout;
            uarts_str.uarts_tx_state++;
            break;
        }
        default :{
            uarts_str.uarts_tx_state=Uart_tx_state_HEADER;
            break;
        }
    }//switch (uarts_str.uarts_tx_state)
}
void UARTS_timeout(void){
    if(uarts_str.timeout_count<=10){
        uarts_str.uarts_rx_state=Uart_rx_state_HEADER;
        // PORTB=1<<4;
    }
    else{
        uarts_str.timeout_count--;
        // PORTB=0;
    }
}
//TIMER
void timer_init(void){
	TCCR1A=(1<<WGM11)|(0<<WGM10);
	TCCR1B=(1<<CS12) |(0<<CS11) |(1<<CS10) |(1<<WGM12)|(1<<WGM13);
	TIMSK1=(1<<TOIE1);//?????????
	//8MHZ/1024/78=100HZ=10MS,,
	//7800 1s
	ICR1=780;
}
//SPI
void spi_init(void){
	//DDR_SPI = (1<<DD_MOSI)|(1<<DD_SCK);
	DDRB=255;//(1<<3)|(1<<5);//SPI
    DDRB&=~(1<<4);
	SPCR=(1<<SPE)|(1<<MSTR)|(1<<SPR0);
	SPCR|=(1<<CPOL)|(1<<CPHA);
}
char spidata(char data){
	SPDR=data;
	while(!(SPSR & (1<<SPIF)))
	;
	data=SPDR;
	return data;
}
//MR
void getdata(void){//USE 0.3ms

    for(int i=0;i<3;i++){
        mrdatatemp[i]=0;
    }
	PORTC=set;//SET
	_delay_us(500);
	PORTC=setx;//////xxx
	spidata(0);
	mrdatatemp[0]|=(spidata(0)<<8);
	mrdatatemp[0]|=spidata(0);
	PORTC=sety;////////yyy
	spidata(0);
	mrdatatemp[1]|=(spidata(0)<<8);
	mrdatatemp[1]|=(spidata(0));
	PORTC=setz;/////////zzz
	spidata(0);
	mrdatatemp[2]|=(spidata(0)<<8);
	mrdatatemp[2]|=(spidata(0));
	PORTC=set;
	PORTC=rset;//RESET
    //wait for more then 5 us
    _delay_us(500);
    for(int i=3;i<6;i++){
        mrdatatemp[i]=0;
    }
	PORTC=rsetx;//////xxx
	spidata(0);
	mrdatatemp[3]|=(spidata(0)<<8);
	mrdatatemp[3]|=(spidata(0));
	PORTC=rsety;////////yyy
	spidata(0);
	mrdatatemp[4]|=(spidata(0)<<8);
	mrdatatemp[4]|=(spidata(0));
	PORTC=rsetz;/////////zzz
	spidata(0);
	mrdatatemp[5]|=(spidata(0)<<8);
	mrdatatemp[5]|=(spidata(0));
	PORTC=rset;
    //ALL DATA/2

    for(int i=0;i<6;i++){
        mrdatatemp[i]=mrdatatemp[i]>>1;
    }

	mrdata[0]=(0x8000+(mrdatatemp[0])-(mrdatatemp[3]));
	mrdata[1]=(0x8000+(mrdatatemp[1])-(mrdatatemp[4]));
	mrdata[2]=(0x8000+(mrdatatemp[2])-(mrdatatemp[5]));

	mrdata[3]=((mrdatatemp[0]/2)+(mrdatatemp[3]/2));
	mrdata[4]=((mrdatatemp[1]/2)+(mrdatatemp[4]/2));
	mrdata[5]=((mrdatatemp[2]/2)+(mrdatatemp[5]/2));
}
//INT
ISR(TIMER1_OVF_vect){
	//getdata();
	//getdataflag=1;
	if(getdataflag==1){
		//PORTD=3;
		//getdataflag=0;
		}
	else{
		//PORTD=0;
		getdataflag=1;
	}

}
ISR((USART_RX_vect)){
	UARTS_rx();
}
ISR((USART_TX_vect)){
	UARTS_tx();
}
