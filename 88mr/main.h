

#define UCSRA   UCSR0A
#define UCSRB   UCSR0B
#define UCSRC   UCSR0C
#define UBRRH   UBRR0H
#define UBRRL   UBRR0L
#define USART_DDR   DDRD
#define USART_PORT  PORTD
#define USART_RX    PD0
#define USART_TX    PD1

//HHHH
#define HEADER 0xAA
#define DATAOK 0x00


#define Maxtemp 16
typedef struct Uarts_Str{
   volatile uint8_t uarts_rx_state;
   volatile uint8_t uarts_tx_state;
   volatile uint8_t uarts_chks;
   volatile uint8_t uarts_UID;
   volatile uint8_t uarts_addr;
   volatile uint8_t uarts_WR;
   volatile uint8_t uarts_byte_count;
   volatile uint8_t uarts_totalbytes;
   volatile uint8_t uarts_masterchk;
   volatile uint32_t timeout_count;
   volatile uint8_t uarts_data_temp[Maxtemp];
} uarts_str_;

volatile uarts_str_ uarts_str;

typedef struct {
    void*   ptr;
    uint8_t bytes;
} register_ptr_str;

volatile register_ptr_str reg[10];

#define Uart_rx_state_HEADER 0
#define Uart_rx_state_UID    1
#define Uart_rx_state_ADDR   2
#define Uart_rx_state_BYTES  3
#define Uart_rx_state_DATA   4
#define Uart_rx_state_CHKSUM 5

#define Uart_tx_state_HEADER 0
#define Uart_tx_state_DATA   1
#define Uart_tx_state_CHKS   2



#define Maxtimeout_count 20000//10ms
#define UDR   UDR0
#define P_UID 0

void USART_set(void);
void UARTS_rx(void);
void UARTS_tx(void);
void UARTS_timeout(void);
